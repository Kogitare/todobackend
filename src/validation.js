// get dependencies
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

// create schemas for validation
const idSchem = Joi.objectId();
const taskSchem = Joi.object({
    name: Joi.string().max(50).required()
});
const modSchem = Joi.object({
    name: Joi.string().max(50),
    done: Joi.boolean()
}).min(1);

// create errors that can occur during validation
const errIdExists = Error('id doesn\'t exist in the database');
const isValidError = Joi.isError;

// export errors for comparisons
exports.errors = {
    errIdExists,
    isValidError
}

// check if id refers to existing task in db
const checkIdExists = async (id, tasks) => {
    // get list of task ids in database
    ids = [];
    await tasks.find({}).forEach((task) => {
        ids.push(task._id);
    });

    // check if id is in that list
    if (ids.some((_id) => _id == id)) {
        return true;
    } else {
        throw errIdExists;
    }
};

// check if id is valid and if it refers to existing task in db
exports.checkId = async (id, tasks) => {
    Joi.assert(id, idSchem);
    await checkIdExists(id, tasks);
}

// check if new task has all needed properties and that they are correct
exports.checkNewTask = (task) => {
    Joi.assert(task, taskSchem);
}

// check if task modification JSON has correct structure
exports.checkTaskMod = async (mod, tasks) => {
    Joi.assert(mod.id, idSchem);
    Joi.assert(mod.modification, modSchem);
    await checkIdExists(mod.id, tasks);
}
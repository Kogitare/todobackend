// get dependencies
const errors = require('./validation').errors;

// send specific http response based on encountered error
exports.handle = (res, err, method) => {
    switch (method) {
        // handle errors for GET
        case 'GET':
            if (errors.isValidError(err)) {
                res.status(422).send(err.details[0].message);
            } else if (err == errors.errIdExists) {
                res.status(404).send(err.message);
            } else {
                res.status(500).send(err.message);
            }
            break;
        
        // handle errors for POST
        case 'POST':
            if (errors.isValidError(err)) {
                res.status(422).send(err.details[0].message);
            } else {
                res.status(500).send(err.message);
            }
            break;
        
        // handle errors for PUT
        case 'PUT':
            if (errors.isValidError(err)) {
                res.status(422).send(err.details[0].message);
            } else if (err == errors.errIdExists) {
                res.status(406).send(err.message);
            } else {
                res.status(500).send(err.message);
            }
            break;
        
        // handle errors for DELETE
        case 'DELETE':
            if (errors.isValidError(err)) {
                res.status(422).send(err.details[0].message);
            } else if (err == errors.errIdExists) {
                res.status(406).send(err.message);
            } else {
                res.status(500).send(err.message);
            }
            break;
    }
}
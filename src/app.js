// get dependencies
const {routing} = require('./routes');
const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');

// return set up express app
exports.setup = (dbConnection) =>
{
    app = express();

    // user cors for cross origin resource sharing
    app.use(cors());

    // use JSON parser to parse body of requests
    app.use(bodyParser.json());

    // set static file access, to show the app
    app.use(express.static('build'));

    // set up routing using database connection
    routing(app, dbConnection); 

    return app;
}
// get dependencies
const mongodb = require('mongodb');
const operations = require('./dbOperations');
const valid = require('../validation');

// create class, that creates object which allows to connect
//  and do operations on mongodb server
class Client {
    // create a mongodb client
    constructor(dbAddress, collName)
    {
        this.dbName = 'tododata'; // database name
        this.collName = collName; // collection name
        const uri = `mongodb://${dbAddress}/${this.dbName}`;
        console.log(`### Connecting to mongodb at: ${uri}`);
        console.log(`### Collection used: ${this.collName}`);
        this.client = new mongodb.MongoClient(uri);
        this.tasks = null; // gets value after calling connect() method
    }

    // connect to database
    async connect()
    {
        try
        {
            await this.client.connect();

            // connect to specific database
            const db = await this.client.db(this.dbName);

            // throw error, when connection to db is lost
            db.on('close', () => {throw Error('Connection lost.')});

            // get specific collection from database
            this.tasks = await db.collection(this.collName);
        } catch(err)
        {
            throw err;
        }
    }

    // return all tasks from database
    async fetchAll()
    {
        return await operations.fetchAll(this.tasks);
    }

    // return specific tasks from database
    async fetchTask(id)
    {
        await valid.checkId(id, this.tasks);
        return await operations.fetchTask(this.tasks, id);
    }

    // return id of new task added to database
    async addTask(task)
    {
        valid.checkNewTask(task);
        return await operations.addTask(this.tasks, task);
    }

    // return id of task modified in database
    async modTask(mod)
    {
        await valid.checkTaskMod(mod, this.tasks);
        return await operations.modTask(this.tasks, mod);
    }

    // remove a specific task (return nothing)
    async delTask(id)
    {
        await valid.checkId(id, this.tasks);
        await operations.delTask(this.tasks, id);
    }

    // disconnect from database
    async close()
    {
        try
        {
            await this.client.close();
        } catch(err)
        {
            throw err
        }
    }
}

// return connected instance of Client class
exports.connectToDb = (dbAddress, collName) =>
{
    const dbConnection = new Client(dbAddress, collName);
    dbConnection.connect();
    return dbConnection;
}
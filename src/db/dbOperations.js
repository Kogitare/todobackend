// get dependencies
const ObjectID = require('mongodb').ObjectID;

// fetch for and return all tasks from db in format:
//  { tasks: [...] }
exports.fetchAll = async (tasks) => {
    try {
        return {tasks: await tasks.find({}).toArray()};
    } catch(err) {
        return err;
    }
}

// fetch for and return specific task from db
exports.fetchTask = async (tasks, id) => {
    try {
        id = new ObjectID(id);
        return await tasks.findOne({_id: id});
    } catch(err) {
        return err;
    }
}

// insert new task to db and return its id in format:
//  { id: ObjectID }
exports.addTask = async (tasks, task) => {
    try {
        task.done = false;
        return {id: (await tasks.insertOne(task)).insertedId};
    } catch(err) {
        return err;
    }
}

// change task data in db and return its id in format:
//  { id: ObjectID }
// 
//  format of mod object:
//  mod = {
//      id: ObjectID,
//      modification: {
//          ...taskMods...
//      }
//  }
exports.modTask = async (tasks, mod) => {
    try {
        id = new ObjectID(mod.id);
        mod = mod.modification;
        return {id: (await tasks.findOneAndUpdate({_id: id}, {$set: mod})).value._id};
    } catch(err) {
        return err;
    }
}

// remove specific task from database and return nothing
exports.delTask = async (tasks, id) => {
    try {
        id = new ObjectID(id);
        await tasks.findOneAndDelete({_id: id});
    } catch(err) {
        return err;
    }
}
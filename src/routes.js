// get dependencies
const errors = require('./errors');

// set all required routings of express app
exports.routing = (app, db) => 
{
    // set GET method to send all of tasks
    app.get('/api/v1/tasks', async (req, res) => 
    {
        try {
            res.status(200).send(await db.fetchAll());
        } catch(err) {
            // send different codes and content based on the error
            errors.handle(res, err, 'GET');
            console.error(err);
        }
    });

    // set GET method to send specific task
    app.get('/api/v1/tasks/:id', async (req, res) => 
    {
        try {
            const id = req.params.id; // get id from url
            res.status(200).send(await db.fetchTask(id));
        } catch(err) {
            // send different codes and content based on the error
            errors.handle(res, err, 'GET');
            console.error(err);
        }
    });

    // ?
    app.get('/api/v1/coffee', (req, res) =>
    {
        res.sendStatus(418);
    });

    // set POST method to add new task and send its id
    app.post('/api/v1/tasks', async (req, res) => 
    {
        try {
            const task = req.body; // get task from request body
            res.status(201).send(await db.addTask(task));
        } catch(err) {
            // send different codes and content based on the error
            errors.handle(res, err, 'POST');
            console.error(err);
        }
    });

    // set PUT method to modify existing task and send its id
    app.put('/api/v1/tasks/:id', async (req, res) => 
    {
        try {
            // get id from url and task modifications from request body
            const mod = {
                id: req.params.id,
                modification: req.body
            };
            res.status(200).send(await db.modTask(mod));
        } catch(err) {
            // send different codes and content based on the error
            errors.handle(res, err, 'PUT');
            console.error(err);
        }
    });

    // set DELETE method to remove a task
    app.delete('/api/v1/tasks/:id', async (req, res) => 
    {
        try {
            const id = req.params.id; // get id from url
            await db.delTask(id)
            res.sendStatus(200);
        } catch(err) {
            // send different codes and content based on the error
            errors.handle(res, err, 'DELETE');
            console.error(err);
        }
    });
}
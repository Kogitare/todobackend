// require environmental variables
require('dotenv').config({path: __dirname + '/var.env'});

// create database connection
const dbAddress = process.env.DB_ADD || 'localhost';
const collName = process.env.COL_NAME || 'tasks';
const dbConnection = require('./db/database').connectToDb(dbAddress, collName);

// configure express app
const app = require('./app').setup(dbConnection);
const port = Number.parseInt(process.env.PORT) || 3000;

// launch app on specific port
app.listen(port, () => 
{
    console.log(`### App listening at: http://localhost:${port}`)
});
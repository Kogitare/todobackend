# "To do" app backend documentation
This file contains useful information about the app.

# Installation and setup
### Database setup
Install MongoDB server WITHOUT ANY USERS or authentication required to connect to it. That's all.

### Backend installation
1. Navigate to folder of your choosing and clone the repository (it will create a new folder containing all files):<br>
```
git clone https://gitlab.com/Kogitare/todobackend.git
```
2. Navigate to the project folder:<br>
```
cd todobackend
```
3. Copy content of `src/var.default.env` file to new `src/var.env` file, and modify its contents:
    1. Specify the port on which app will be launched on (`PORT` variable, default is '3000')
    2. Specify address of the mongodb (`DB_NAME` variable, default is 'localhost')
    3. Specify collection name that app should use for operations (`COL_NAME` variable, default is 'tasks').
4. While in main directory of the app (`todobackend`), install all dependencies by running this command:<br>
```
npm install
```

### Initialize and start the backend
To start the app, be in the main directory of the app (`todobackend`) and run this command:<br>
```
npm start
```

# Endpoints

<table>
    <tr>
        <th>method</th>
        <th>endpoint</th>
        <th>requires</th>
        <th>result</th>
        <th>description</th>
    </tr>
    <tr>
        <td style="text-align:right">GET</td>
        <td style="text-align:left"><code>/api/v1/tasks</code></td>
        <td>
            -
        </td>
        <td>
            JSON with task array:
<pre>
{
    tasks: [...]
}
</pre>
        </td>
        <td>
            Gets all of tasks from the database.
        </td>
    </tr>
    <tr>
        <td style="text-align:right">GET</td>
        <td style="text-align:left"><code>/api/v1/tasks/<em>id</em></code></td>
        <td>
            1. specifying <code>id</code> in url
        </td>
        <td>
            JSON representing a task:
<pre>
{
    _id: ObjectID,
    name: "max 50 characters",
    done: boolean
}
</pre>
        </td>
        <td>
            Gets a specific task from the database.
        </td>
    </tr>
    <tr>
        <td style="text-align:right">POST</td>
        <td style="text-align:left"><code>/api/v1/tasks</code></td>
        <td>
            1. JSON representing a task* in request <code>body</code>
<pre>
{
    name: "max 50 characters"
}
</pre>
            <em>* you don't specify <code>id</code> and <code>done</code> - they will be added automatically</em>
        </td>
        <td>
            JSON with id of created task:
<pre>
{
    id: ObjectID
}
</pre>
        </td>
        <td>
            Sends a new task to be saved in the database.
        </td>
    </tr>
    <tr>
        <td style="text-align:right">PUT</td>
        <td style="text-align:left"><code>/api/v1/tasks/<em>id</em></code></td>
        <td>
            1. specifying <code>id</code> in url <br>
            2. JSON representing task modifications* in request <code>body</code>
<pre>
{
    name: "max 50 characters",
    done: boolean
}
</pre>
            <em>* you need to specify at least one property, of the <code>task</code> JSON (excluding <code>_id</code> and <code>done</code>)</em>
        </td>
        <td>
            JSON with id of changed task:
<pre>
{
    id: ObjectID
}
</pre>
        </td>
        <td>
            Modifies task that exists in the database.
        </td>
    </tr>
    <tr>
        <td style="text-align:right">DELETE</td>
        <td style="text-align:left"><code>/api/v1/tasks/<em>id</em></code></td>
        <td>
            1. specifying <code>id</code> in url
        </td>
        <td>
            -
        </td>
        <td>
            Removes task from the database.
        </td>
    </tr>
</table>

# Files & directories
### File dependency structure in src/
![File dependency structure in src directory.](./documentation/fileStructure.png)
